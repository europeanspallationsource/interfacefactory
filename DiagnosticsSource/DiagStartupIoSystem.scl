﻿FUNCTION_BLOCK "DiagStartupIoSystem"
{ S7_Optimized_Access := 'TRUE' }
VERSION : 0.1
   VAR_INPUT DB_SPECIFIC
      ioSystemHwId : HW_IOSYSTEM;   // This ID is representing the IO System (PN or DP), find the ID in the system constants
   END_VAR

   VAR_OUTPUT 
      status : Int;   // The return value of system function, where the last error occured
      instructionError : Int;   // Indicates in which system function the error occured: 1= DeviceStates PN , 2=GetName PN, 3=ModuleStates PN,  4=DeviceStates DP, 5= GetName DP. 6= ModuleStates DP
      errorIndex : Int;   // The last index of the respective loop, if an error occures
   END_VAR

   VAR_IN_OUT DB_SPECIFIC
      errorState { S7_HMI_Accessible := 'False'; S7_HMI_Visible := 'False'} : Bool;   // if this is TRUE one of the modules in the IO system has a problem
      errorType { S7_HMI_Accessible := 'False'; S7_HMI_Visible := 'False'} : Byte;   // Status of  the IO system 1=OK, 2=faulty, 3=lost connection, 4=disabled
      ioSystemAddress { S7_HMI_Accessible := 'False'; S7_HMI_Visible := 'False'} : HW_DEVICE;   // Logical address of a device or slave (system constant in HW configuration)
      ioSystemId { S7_HMI_Accessible := 'False'; S7_HMI_Visible := 'False'} : UInt;   // ID of  IO System
   END_VAR
   VAR_IN_OUT 
      ioSystemname : String;   // Device name
   END_VAR
   VAR_IN_OUT DB_SPECIFIC
      ActuallyConfiguredModules { S7_HMI_Accessible := 'False'; S7_HMI_Visible := 'False'} : Int;   // Number of configured local IO cards (PN or DP head modul included)
   END_VAR
   VAR_IN_OUT 
      StateOfModules : Array[1.."MAX_MODULES_IN_IO_DEVICE"] of Bool;   // Status of the Modules FALSE=OK, TRUE=faulty,
      AddressOfModules : Array[1.."MAX_MODULES_IN_IO_DEVICE"] of HW_IO;   // Detected HW address if zero = module doesn't exists
   END_VAR

   VAR 
      statGeoAddr {OriginalPartName := 'GEOADDR'; LibVersion := '1.0'; S7_HMI_Accessible := 'False'; S7_HMI_Visible := 'False'} : GEOADDR;   // Slot information
      statGeoLaddr { S7_HMI_Accessible := 'False'; S7_HMI_Visible := 'False'} : HW_ANY;   // GEO2LOG hardware identifier
      statConfiguredDevices { S7_HMI_Accessible := 'False'; S7_HMI_Visible := 'False'} : Array[0..1023] of Bool;   // Temporary storage of the return of "DeviceStates", to combine the states of the devices with numbers and names
      statExistingDevices : Array[0..1023] of Bool;   // Temporary storage of the return of "DeviceStates", to combine the states of the devices with numbers and names
      statFaultyDevices : Array[0..1023] of Bool;   // Temporary storage of the return of "DeviceStates", to combine the states of the divices with numbers and names
      statDisabledDevices { S7_HMI_Accessible := 'False'; S7_HMI_Visible := 'False'} : Array[0..1023] of Bool;   // Storage of the status of all devices in the PN IO System --> State: Disabled
      statProblemDevices { S7_HMI_Accessible := 'False'; S7_HMI_Visible := 'False'} : Array[0..1023] of Bool;   // Storage of the status of all devices in the PN IO System --> State: Problem
      statDeviceModuleStates : Array[0..127] of Bool;   // Storage of the status of all modules in the PN Devices --> State: Problem
      instGetNameDevices {OriginalPartName := 'FB_806_S71500'; LibVersion := '1.3'} : Get_Name;   // Instance of system function "GetName"
      statInitString : String;   // Used to initialize the temporary string to convert into STRING[50]
      statResetStatesOld : Bool;   // Detect a rising edge at ResetStates
   END_VAR

   VAR_TEMP 
      tempSlotIndex : Int;   // Loop index
      tempModuleNum : Int;   // index module number
      tempRetValGeo : Int;   // GEO2LOG error information
      tempRetValDeviceStates : Int;   // DeviceStates error information
      tempRetValModuleStates : Int;   // Return value system function ModuleStates
      tempStringConvert : String;   // Store the device names temporary here, to convert them into STRING[50]
      tempIoSystemError : Bool;
   END_VAR

   VAR CONSTANT 
      STATE_CONFIGURED : USInt := 1;   // Used for instruction DeviceStates, read out all configured devices
      STATE_FAULTY : USInt := 2;   // Used for instruction DeviceStates, read out all faulty devices
      STATE_DISABLED : USInt := 3;   // Used for instruction DeviceStates, read out all disabled devices
      STATE_EXIST : USInt := 4;   // Used for instruction DeviceStates, read out all devices not reachable
      STATE_PROBLEM : USInt := 5;   // Used for instruction DeviceStates, read out all devices with several problems
      DEVICE_SLAVE : USInt := 2;   // GEO2LOG structure: HW type = 2
      MODULE_OF_DEVICE : USInt := 4;   // GEO2LOG structure: HW type = 4
      IO_SYSTEM_AREA : USInt := 1;   // GEO2LOG structure: Area = 1
      DP_SYSTEM_AREA : USInt := 2;   // GEO2LOG structure: Area = 2
      ERR_DEV_STATE_DEVICES : USInt := 1;   // Identifies the instruction behind the error code of output "Status" --> DeviceStates PN (Configured, faulty, existing)
      ERR_GET_NAME_DEVICES : USInt := 2;   // Identifies the instruction behind the error code of output "Status" -->  GetName of devices PN
      ERR_MOD_STAT_DEVICES : USInt := 3;   // Identifies the instruction behind the error code of output "Status" --> ModuleStates PN
      ERR_DEV_STAT_PN : USInt := 1;   // Value for output instruction error, DeviceStates PN devices
      ERR_MOD_STAT_PN : USInt := 4;   // Value for output instruction error, ModuleStates PN devices
      MAX_SLAVES_DP : Int := 127;
   END_VAR


BEGIN
	//=============================================================================
	//Author: Miklos Boros
	//        email: borosmiklos@gmail.com; miklos.boros@esss.se
	//
	//Functionality: Determine hardware identifier and module states from 
	//               PN or DP IO system
	// 
	//
	//Please Note:
	//        This block is part of the generated ESS Standard Code
	//        DON'T CHANGE THIS BLOCK MANUALY. Any changes will be overwritten in the next code generation.
	//
	//
	//Change log table:
	//Version  Date         Expert in charge      Changes applied
	//01.00.00 29.08.2017   ESS/ICS               First released version for InterfaceFactory 
	//=============================================================================
	
	
	//=============================================================================
	// CONFIGURED DEVICES
	//=============================================================================
	// Find out how much devices are configured in the IO System --> PROFINET IO
	// This number is the maximum number of devices, which will be checked in the
	// following programm
	#tempRetValDeviceStates := DeviceStates(LADDR := #ioSystemHwId,
	                                        MODE := #STATE_CONFIGURED,
	                                        STATE := #statConfiguredDevices);
	
	// Check if the block call was successful
	IF (#tempRetValDeviceStates <> 0)
	THEN
	  // Error handling
	  #status := #tempRetValDeviceStates;
	  #instructionError := #ERR_DEV_STATE_DEVICES;
	  // Call ok --> store the actual number of configured devices    
	ELSE
	  ;
	END_IF;
	
	//=============================================================================
	// EXISTING DEVICES
	//=============================================================================
	#tempRetValDeviceStates := DeviceStates(LADDR := #ioSystemHwId,
	                                        MODE := #STATE_EXIST,
	                                        STATE := #statExistingDevices);
	
	// Check if the block call was successful
	IF (#tempRetValDeviceStates <> 0)
	THEN
	  // Error handling
	  #status := #tempRetValDeviceStates;
	  #instructionError := #ERR_DEV_STATE_DEVICES;
	  // Call ok --> store the actual number of configured devices    
	ELSE
	  ;
	END_IF;
	
	//=============================================================================
	// FAULTY DEVICES
	//=============================================================================
	#tempRetValDeviceStates := DeviceStates(LADDR := #ioSystemHwId,
	                                        MODE := #STATE_FAULTY,
	                                        STATE := #statFaultyDevices);
	
	// Check if the block call was successful
	IF (#tempRetValDeviceStates <> 0)
	THEN
	  // Error handling
	  #status := #tempRetValDeviceStates;
	  #instructionError := #ERR_DEV_STATE_DEVICES;
	  // Call ok --> store the actual number of configured devices    
	ELSE
	  ;
	END_IF;
	
	// Find out the number of the assigned IO system, to define if it is
	// a PN or DP Network
	#tempRetValGeo := LOG2GEO(LADDR := #ioSystemHwId,
	                          GEOADDR := #statGeoAddr);
	
	// set IO system ID
	#ioSystemId := #statGeoAddr.IOSYSTEM;
	
	// Inilize the structure for system function GEO2LOG
	#statGeoAddr.HWTYPE := #DEVICE_SLAVE;   // Hardware type 2: IO device
	// Predefine the type OF IO system. Either Profinet IO or Profibus DP
	IF ((#statGeoAddr.IOSYSTEM >= 100) 
	  AND (#statGeoAddr.IOSYSTEM <= 115))
	THEN
	  #statGeoAddr.AREA := #IO_SYSTEM_AREA;   // Area ID 1: PROFINET IO
	ELSIF ((#statGeoAddr.IOSYSTEM >= 1)
	  AND (#statGeoAddr.IOSYSTEM <= 32))
	THEN
	  #statGeoAddr.AREA := #DP_SYSTEM_AREA;   // Area ID 2: Profibus DP
	ELSE
	  ;
	END_IF;
	
	// The devices are configured --> Read out the logical address and the
	// device name
	IF (#statConfiguredDevices[1] = TRUE)
	THEN
	    
	    
	    // Station number 
	    #statGeoAddr.STATION := 1;
	    // read LADDR from devices
	    #tempRetValGeo := GEO2LOG(GEOADDR := #statGeoAddr,
	                              LADDR => #statGeoLaddr);
	    // Everything is ok!
	    IF (#tempRetValGeo = 0)
	    THEN
	        // store LADDR from devices in diagnostic data block
	        #ioSystemAddress := #statGeoLaddr;
	        
	        // Store the device name, if the device is existing
	        // Get name is an acyclic instruction. In the startup OB, the
	        // instruction has to be repeated until it is done or error
	        REPEAT
	            // Get device name of each decive in PN System
	            #instGetNameDevices(LADDR := #ioSystemHwId,
	                                STATION_NR := #statGeoAddr.STATION,
	                                DATA := #tempStringConvert);
	            
	        UNTIL (#instGetNameDevices.DONE = TRUE)
	            OR (#instGetNameDevices.ERROR = TRUE)
	        END_REPEAT;
	        
	        IF (#instGetNameDevices.ERROR = TRUE)
	        THEN
	            // Error handling
	            #status := WORD_TO_INT(#instGetNameDevices.STATUS);
	            #instructionError := #ERR_GET_NAME_DEVICES;
	            #errorIndex := 1;
	            
	            // Everything is ok --> Convert the String[254] into String[50]    
	        ELSIF (#instGetNameDevices.DONE = TRUE)
	        THEN
	            // Cut all characters more than 50 to reduce the string length
	            #ioSystemname := DELETE(IN := #tempStringConvert,
	                                    L := 204,
	                                    P := 50);
	            
	            // Initialize the temporary string before next loop
	            #tempStringConvert := #statInitString;
	        ELSE
	            ;
	        END_IF;
	    ELSE
	        // If the return value ist not = 0 --> the device/system/module is
	        // not configured --> No error handling
	        // set LADDR from devices to 0 in diagnostic data block
	        #ioSystemAddress := 0;
	        #ioSystemname := '';
	    END_IF;
	    
	    // Check if the configured devices are faulty or lost once through
	    // the startup!
	    IF (#statExistingDevices[1] = TRUE)
	    THEN
	        IF (#statFaultyDevices[1] = TRUE)
	        THEN
	            
	            #errorType := 2;
	            #errorState := TRUE;
	            
	            // The device is not faulty and does exist --> set state ok!    
	        ELSE
	            #errorType := 1;
	            #errorState := FALSE;
	        END_IF;
	        // The connection to the device is lost at the moment    
	    ELSE
	        #errorType := 3;
	        #errorState := TRUE;
	    END_IF;
	    // No device is configured   
	ELSE
	    ;
	END_IF;
	
	
	
	//=============================================================================
	// Get the logical address of all modules and store the actual number of
	// modules from each device
	//=============================================================================
	
	// Inilize the structure for system function GEO2LOG
	#statGeoAddr.HWTYPE := #MODULE_OF_DEVICE;   // Hardware type 4: Module
	FOR #tempModuleNum := 1 TO "MAX_MODULES_IN_IO_DEVICE" DO
	    // Slot number
	    #statGeoAddr.SLOT := INT_TO_UINT(#tempModuleNum);
	    // read LADDR from modules
	    #tempRetValGeo := GEO2LOG(GEOADDR := #statGeoAddr,
	                              LADDR => #statGeoLaddr);
	    // check Retval
	    IF (#tempRetValGeo = 0)
	    THEN
	        // store LADDR from modules in diagnostic data block
	        #AddressOfModules[#tempModuleNum] := #statGeoLaddr;
	        #ActuallyConfiguredModules := #ActuallyConfiguredModules + 1;
	    ELSE
	        // If the return value ist not = 0 --> the device/system/module is
	        // not configured --> No error handling
	        // set LADDR from modules to 0 in diagnostic data block
	        #AddressOfModules[#tempModuleNum] := 0;
	    END_IF;
	END_FOR;
	
	//=============================================================================
	// Check modules of faulty Devices
	//=============================================================================
	
	// Check the state of the configured devices
	IF (#errorType = 2)
	THEN
	    // The device is reachable, but faulty because of an error in a
	    // subordinated system --> check the modules
	    #tempRetValModuleStates := ModuleStates(LADDR := #ioSystemAddress,
	                                            MODE := #STATE_PROBLEM,
	                                            STATE := #statDeviceModuleStates);
	    
	    IF (#tempRetValModuleStates <> 0)
	    THEN
	        // Error hanlding
	        #status := #tempRetValModuleStates;
	        #instructionError := #ERR_MOD_STAT_DEVICES;
	    ELSE
	        // Store the state of the different module in the diag DB
	        FOR #tempSlotIndex := 1 TO #ActuallyConfiguredModules DO
	            IF (#statDeviceModuleStates[#tempSlotIndex] = TRUE)
	            THEN
	                #StateOfModules[#tempSlotIndex] := TRUE;
	            ELSE
	                #StateOfModules[#tempSlotIndex] := FALSE;
	            END_IF;
	        END_FOR;
	    END_IF;
	ELSIF (#errorType = 3)
	THEN
	    
	    FOR #tempSlotIndex := 1 TO #ActuallyConfiguredModules DO
	        IF (#AddressOfModules[#tempSlotIndex] <> 0)
	        THEN
	            #StateOfModules[#tempSlotIndex] := TRUE;
	        END_IF;
	    END_FOR;
	    
	ELSE
	    ; // no faulty device --> nothing to do!
	END_IF;
	
	
	
END_FUNCTION_BLOCK

