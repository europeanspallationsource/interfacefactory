﻿FUNCTION "PullOrPlugModules" : Void
{ S7_Optimized_Access := 'TRUE' }
VERSION : 0.1
   VAR_INPUT 
      laddr : HW_IO;   // Hardware identifier
      eventClass : Byte;   // 16#38/39: module inserted, removed
      faultId : Byte;   // Fault identifier
   END_VAR

   VAR_TEMP 
      tempGeoAddr {OriginalPartName := 'GEOADDR'; LibVersion := '1.0'} : GEOADDR;   // Geographical address of the disturbed Module / Device
      tempRetVal : Int;   // Return value of LOG2GEO
   END_VAR

   VAR CONSTANT 
      AREA_CENTRAL : UInt := 0;   // Area ID for PLC
      AREA_PROFINET : UInt := 1;   // Area ID for PROFINET IO
      AREA_PROFIBUS : UInt := 2;   // Area ID for PROFIBUS DP
      MODULE_PLUGGED : Byte := 16#38;   // (Sub)module plugged
      MODULE_PULLED : Byte := 16#39;   // (Sub)module pulled or not responding
      MODULE_MATCHES : Byte := 16#54;   // IO submodule inserted and matches configured submodule
      MODULE_OK : Byte := 16#61;   // Module inserted, module type OK
   END_VAR


BEGIN
	//=============================================================================
	//Author: Miklos Boros
	//        email: borosmiklos@gmail.com; miklos.boros@esss.se
	//
	//Functionality: Evaluate Pull/plug interrupt OB information for PLC and
	//               devices
	// 
	//
	//Please Note:
	//        This block is part of the generated ESS Standard Code
	//        DON'T CHANGE THIS BLOCK MANUALY. Any changes will be overwritten in the next code generation.
	//
	//
	//Change log table:
	//Version  Date         Expert in charge      Changes applied
	//01.00.00 29.08.2017   ESS/ICS               First released version for InterfaceFactory 
	//=============================================================================
	
	#tempRetVal := LOG2GEO(LADDR := #laddr,
	                       GEOADDR := #tempGeoAddr);
	
	//=============================================================================
	// evaluate diagnosis information for devices in an IO system
	//=============================================================================
	IF (#tempGeoAddr.AREA = #AREA_PROFINET)
	    OR (#tempGeoAddr.AREA = #AREA_PROFIBUS)
	THEN
	
	    //=============================================================================
	    // Check IO system 1 
	    //=============================================================================
	    IF "DiagnosticsData".IOSystem1.ioSystemId = #tempGeoAddr.IOSYSTEM
	    THEN
	        IF (#tempGeoAddr.STATION <= "MAX_IO_DEVICES")
	            AND (#tempGeoAddr.SLOT+1 <= "MAX_MODULES_IN_IO_DEVICE")
	        THEN
	            // check modules plugged
	            IF (#eventClass = #MODULE_PLUGGED)
	            THEN
	                // reset error state only if the correct module is inserted
	                IF (#faultId = #MODULE_MATCHES)
	                    OR (#faultId = #MODULE_OK)
	                THEN
	                    "DiagnosticsData".IOSystem1.errorState := False;
	                    "DiagnosticsData".IOSystem1.StateOfModules[#tempGeoAddr.SLOT+1] := false;
	                END_IF;
	                
	                // check modules pulled  
	            ELSIF (#eventClass = #MODULE_PULLED)
	            THEN
	                "DiagnosticsData".IOSystem1.errorState := TRUE;
	                "DiagnosticsData".IOSystem1.errorType := 2;
	                "DiagnosticsData".IOSystem1.StateOfModules[#tempGeoAddr.SLOT+1] := true;
	                
	            ELSE
	                ;
	            END_IF;
	        END_IF;
	        
	    END_IF;
	    
	    //=============================================================================
	    // evaluate diagnosis information for PLC
	    //=============================================================================
	ELSIF (#tempGeoAddr.AREA = #AREA_CENTRAL)
	THEN
	    
	    IF (#tempGeoAddr.SLOT+1 <= "MAX_LOCAL_MODULES")
	    THEN
	        IF (#eventClass = #MODULE_PLUGGED)
	        THEN
	            IF (#faultId = #MODULE_MATCHES)
	                OR (#faultId = #MODULE_OK)
	            THEN
	                "DiagnosticsData".MainPLC.StateOfModules[#tempGeoAddr.SLOT+1] := false;
	            END_IF;
	            
	        ELSIF (#eventClass = #MODULE_PULLED)
	        THEN
	            "DiagnosticsData".MainPLC.StateOfModules[#tempGeoAddr.SLOT+1] := true;
	        ELSE
	            ;
	        END_IF;
	        
	        
	    END_IF;
	    
	ELSE
	    ;
	END_IF;
	
	
	
END_FUNCTION

