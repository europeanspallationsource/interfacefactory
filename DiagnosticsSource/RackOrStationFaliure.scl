﻿FUNCTION "RackOrStationFaliure" : Void
{ S7_Optimized_Access := 'TRUE' }
VERSION : 0.1
   VAR_INPUT 
      laddr : HW_IO;   // Hardware identifier
      eventClass : Byte;   // Event class
      faultId : Byte;   // Fault identifier
   END_VAR

   VAR_TEMP 
      index : Int;   // index system
      tempSlotIndex : Int;   // Loop index
      tempIoSystemIndex : Int;   // Index for IO System
      tempGeoAddr {OriginalPartName := 'GEOADDR'; LibVersion := '1.0'} : GEOADDR;   // Geographical address of the disturbed Module / Device
      tempRetVal : Int;   // Return value of LOG2GEO
      tempRetValModuleStates : Int;   // Return value system function ModuleStates
      tempDeviceModuleStates : Array[0..127] of Bool;   // Storage of the status of all modules in the PN Devices --> State: Problem
      tempLaddr : HW_DEVICE;   // logical address of the disturbed Module / Device
   END_VAR

   VAR CONSTANT 
      AREA_CENTRAL : UInt := 0;   // Area ID for PLC
      AREA_PROFINET : UInt := 1;   // Area ID for PROFINET IO
      AREA_PROFIBUS : UInt := 2;   // Area ID for PROFIBUS DP
      SLAVE_DEVICE_RET : Byte := 16#38;   // return of a DP slave / IO device
      SLAVE_DEVICE_FAIL : Byte := 16#39;   // Failure of a DP slave / IO device
      DP_SLAVE_FAIL_RET : Byte := 16#C4;   // Failure/return of a DP slave
      IO_DEVICE_FAIL_RET : Byte := 16#CB;   // Failure/return of a PROFINET IO device
      STATE_PROBLEM : USInt := 5;   // Used for instruction DeviceStates, read out all devices with several problems
   END_VAR


BEGIN
	//=============================================================================
	//Author: Miklos Boros
	//        email: borosmiklos@gmail.com; miklos.boros@esss.se
	//
	//Functionality: Evaluate Rack/Station interrupt OB information for PLC and
	//               devices
	// 
	//
	//Please Note:
	//        This block is part of the generated ESS Standard Code
	//        DON'T CHANGE THIS BLOCK MANUALY. Any changes will be overwritten in the next code generation.
	//
	//
	//Change log table:
	//Version  Date         Expert in charge      Changes applied
	//01.00.00 29.08.2017   ESS/ICS               First released version for InterfaceFactory 
	//=============================================================================
	
	//=============================================================================
	// evaluate event class from Rack error OB
	// and set information to DiagnosticsData DB
	//=============================================================================
	
	//=============================================================================
	// determine geographic address of faulty device
	//=============================================================================
	#tempRetVal := LOG2GEO(LADDR := #laddr,
	                       GEOADDR := #tempGeoAddr);
	
	//=============================================================================
	// evaluate diagnosis information for devices in an IO system
	//=============================================================================
	IF (#tempGeoAddr.AREA = #AREA_PROFINET)
	    OR (#tempGeoAddr.AREA = #AREA_PROFIBUS)
	THEN
	    
	    //=============================================================================
	    // Check IO system 1 
	    //=============================================================================
	    IF "DiagnosticsData".IOSystem1.ioSystemId = #tempGeoAddr.IOSYSTEM
	    THEN
	        // check DP slave or IO device return 
	        IF (#eventClass = #SLAVE_DEVICE_RET)
	        THEN
	            // reset error state only if the slave or device state is OK
	            IF (#faultId = #DP_SLAVE_FAIL_RET)
	                OR (#faultId = #IO_DEVICE_FAIL_RET)
	            THEN
	                "DiagnosticsData".IOSystem1.errorState := FALSE;
	                
	                FOR #tempSlotIndex := 1 TO "DiagnosticsData".IOSystem1.ActuallyConfiguredModules DO
	                    "DiagnosticsData".IOSystem1.StateOfModules[#tempSlotIndex] := FALSE;
	                END_FOR;
	                
	            ELSE
	                
	                // The device is reachable, but faulty because of an error in a
	                // subordinated system --> check the modules
	                #tempRetVal := GEO2LOG(LADDR => #tempLaddr,
	                                       GEOADDR := #tempGeoAddr);
	                
	                #tempRetValModuleStates := ModuleStates(LADDR := #tempLaddr,
	                                                        MODE := #STATE_PROBLEM,
	                                                        STATE := #tempDeviceModuleStates);
	                
	                IF (#tempRetValModuleStates = 0)
	                THEN
	                    // Store the state of the different module in the diag DB
	                    FOR #tempSlotIndex := 1 TO "DiagnosticsData".IOSystem1.ActuallyConfiguredModules DO
	                        IF (#tempDeviceModuleStates[#tempSlotIndex ] = TRUE)
	                        THEN
	                            "DiagnosticsData".IOSystem1.StateOfModules[#tempSlotIndex] := TRUE;
	                        ELSE
	                            "DiagnosticsData".IOSystem1.StateOfModules[#tempSlotIndex] := FALSE;
	                        END_IF;
	                    END_FOR;
	                END_IF;
	                
	                "DiagnosticsData".IOSystem1.errorType := 2;
	                
	            END_IF;
	            
	            // check DP slave or IO device failure 
	        ELSIF (#eventClass = #SLAVE_DEVICE_FAIL)
	        THEN
	            "DiagnosticsData".IOSystem1.errorState := TRUE;
	            "DiagnosticsData".IOSystem1.errorType := 3;
	            
	            FOR #tempSlotIndex := 1 TO "DiagnosticsData".IOSystem1.ActuallyConfiguredModules DO
	                IF "DiagnosticsData".IOSystem1.AddressOfModules[#tempSlotIndex] <> 0
	                THEN
	                    "DiagnosticsData".IOSystem1.StateOfModules[#tempSlotIndex] := TRUE;
	                END_IF;
	            END_FOR;
	            
	        ELSE
	            ;
	        END_IF;
	        
	    END_IF;
	    
	    //=============================================================================
	    //evaluate diagnosis information for PLC
	    //=============================================================================
	ELSIF (#tempGeoAddr.AREA = #AREA_CENTRAL)
	THEN
	    
	    IF (#tempGeoAddr.SLOT+1 <= "MAX_LOCAL_MODULES")
	    THEN
	        // check DP slave or IO device return 
	        IF (#eventClass = #SLAVE_DEVICE_RET)
	        THEN
	            IF (#faultId = #DP_SLAVE_FAIL_RET)
	                OR (#faultId = #IO_DEVICE_FAIL_RET)
	            THEN
	                "DiagnosticsData".MainPLC.StateOfModules[#tempGeoAddr.SLOT+1] := false;
	            END_IF;
	            
	            // check DP slave or IO device failure 
	        ELSIF (#eventClass = #SLAVE_DEVICE_FAIL)
	        THEN
	            "DiagnosticsData".MainPLC.StateOfModules[#tempGeoAddr.SLOT+1] := true;
	        ELSE
	            ;
	        END_IF;
	        
	        
	    END_IF;
	    
	ELSE
	    ;
	END_IF;
	
	
END_FUNCTION

