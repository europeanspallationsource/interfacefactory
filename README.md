# README #
						
	Interface factory v2.0					
	created 2017 by Miklos Boros					
	About					
	The purpose of this application to visualize the Status , Command and Parameter arrays and to generate the correspondent SCL communication block for the Siemens S71500 program for TIA Portal.					
						
	Usage					
						
	1. Go to the tab OpenDataBaseStructure					
	2. Click on the [Open Database Structure] button					
	3. An open file dialog will be opened 					
	4. Search for the .zip file from PLC Factory output directory (previous steps: .def file creation, TemplateFactory, CCDB update, PLCFactory) described in a separate document					
	5. Click [OPEN]  					
						
	Interface Factory will generate a new folder next to the .zip file with the same name					
	In this folder you find the RegisterDocumentation.xlsx which is the visual representation of all the device types, status/command/parameter mappings					
	you also find InterfaceFactory_external_source.scl file that can be imported to TIAPortal					
						
	Diagnostic checkbox					
						
	If you click the [Generate diagnostics] checkbox you can generate also I/O module diagnostics for your PLC project. 					
	The output block for your PLC logic is a golbal DB called [DiagnosticsData]					
	The configuration for the diagnostics comes from CCDB. The PLC datatype has three parameters: 					
		MAX_IO_DEVICES				Maximum number of PN or DP devices in the configuration
		MAX_LOCAL_MODULES				Maximum number of local modules after the PLC
		MAX_MODULES_IN_IO_DEVICE				Maximum number of modules after a DP or PN head device
						
	Overwrite EPICS DEVICE CALL.FC checkbox					
						
	If you check the [Overwrite EPICS DEVICE CALL.FC] checkbox IterfaceFactory will overwrite this FC during the TIA External Source Compile. 					
	This means that all configured "connection" between the PLC logic and the IOC communication devices will be set back to default.					
	Recommendation: Use this option only the first time when you create your project. 					
						
						
	Compile the PLC code					
						
	1. Make sure that you are using TIA V13 or above, and configured a S71500 type PLC					
	2. Copy the [ESS Standard PLC Code] library into your PLC code					
	3. Clear any previous External Source File from TIA portal					
	4. Import the InterfaceFactory_external_source.scl from the folder created by this application					
	5. Right-click on the InterfaceFactory_external_source file in TIA and select [Generate blocks from source]					
		A few block should appear without any error				
		Instance DBs with the name: DEV_<device name in CCDB>_iDB				
		DEVICETYPE FBs with the name DEVTYPE_<device type name in CCDB>				
		EPICS_device_calls FC				
		EPICS_device_calls_test FC				
		updated TIA MAP in the [ESS Standard PLC code]				
		EPICS_PLC_Tester DB				
	6. Call the block EPICS_device_calls.FC from the MainCyclic FC after the _COMMS call					
	7. Finally you should put all the related variables from the PLC logic to the inputs and outputs of the devices in the EPICS_device_calls					
	8. Done. Recompile the PLC project					
						
	Edit main page in this Excel Application					
	The password for editing the Main Form is: Qwert123456					
